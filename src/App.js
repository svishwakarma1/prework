import React, { Component } from 'react';
import { Router, Route, Redirect, Switch, withRouter } from 'react-router-dom';
import Login from './Login.js';
import Home from './Home.js';

import history from './services/history';

class App extends Component {
  render(){
    return(
      <Router history={history}>
        <Route path="/" exact component = {Login} />
        <Route path="/home" exact component={Home} />
      </Router>
    )
  }
}
export default App;
