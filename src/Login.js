import React, { Component } from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { Button, FormGroup, FormControl } from "react-bootstrap";

import history from './services/history';
import Api from './services/Api';
import Credential from './services/Credential';



class Login extends Component {
	
	constructor(props){
		super(props);
		this.state = {
			email:'',
			password:'',
			isLogin:false
		}
	}

	componentDidMount(){
		history.push('/home');
		// console.warn(history);
	}

	setEmail = (text) => {
		this.setState({email:text});
	}

	setPassword = (password) => {
		this.setState({password:password});
	}

	validateForm = () => {
		let { email, password } = this.state;
	    return email.length > 0 && password.length > 0;
	}

	handleSubmit = (event) => {
	    event.preventDefault();

	   	let { email, password } = this.state;
	   	if( email == Credential.email && password == Credential.password ){
	   		this.setState({isLogin:true})
	   	}else{
	   		alert('User login id and password does not match');
	   	}
	}



	render(){

		let { email, password, data, images, isLogin } = this.state;

		if (isLogin) {
	      return <Redirect to='/home' />
	    }

		return(
			<div  style={{display:'flex',justifyContent:'center',alignItems:'center',paddingTop:'20%'}}>
				<div className="col-md-4" >
			      	<form onSubmit={this.handleSubmit}>
			        <FormGroup controlId="email" bsSize="large">
			          Email
			          <FormControl
			            autoFocus
			            type="email"
			            value={email}
			            onChange={e => this.setEmail(e.target.value)}
			          />
			        </FormGroup>
			        <FormGroup controlId="password" bsSize="large">
			         Password
			          <FormControl
			            value={password}
			            onChange={e => this.setPassword(e.target.value)}
			            type="password"
			          />
			        </FormGroup>
			        <Button block disabled={!this.validateForm()} type="submit">
			          Login
			        </Button>
			      	</form>
			    </div>
			</div>
		)
	}
}


export default withRouter(Login);


