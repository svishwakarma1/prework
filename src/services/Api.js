import axios from 'axios';
import Config from './Config';

// Axios Instance
export default axios.create({
    baseURL: `${Config.baseURL}` 
});