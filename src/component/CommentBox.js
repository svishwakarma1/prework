import React from 'react';
import { Button } from "react-bootstrap";

const CommentBox = (props) => {

	let { comment, logoutFunc } = props;

	comment = comment || [];

	return(
		<div className="popup" id="myForm">
			<div className="form-container">
			    <label><b>Your Comments:</b></label>
			   	{
			   	    comment.map((detail,index)=>{
			   			return(
			   				<div className="row" key={index}>
	  							<div className="col-md-5">
				      				[You] - { detail.comment }
				      			</div>
				      			<div className="col-md-5">
				      				[Input ID] { detail.id }
				      			</div>
	  						</div>
			   			)
			   		})
			   	}

			   	{
			   		comment.length == 0 &&
			   			<div align="center">
			   				<label>empty</label>
			   			</div>
			   	}
			  	
			   	<Button onClick={()=>{
				   		if(logoutFunc)
				   			logoutFunc();
				   	}}
			   		primary={true}
			   	>
			   		Log Out
			   	</Button>
		    </div>
		</div>
	)
}

export default CommentBox;