import React,{ Component } from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';
import { Button, FormGroup, FormControl } from "react-bootstrap";

import CommentBox from './component/CommentBox';
import Api from './services/Api';
import Config from './services/Config';

class Home extends Component {

	constructor(props){
		super(props);
		this.state = {
			imageID:'',
			images:[],
			imageHeight:200,
			imageWidth:200,
			comment:'',
			username:'',
			access_token:'',
			refresh_token:'',
			authorizeView:'',
			userAuthorized:false,
			userComments:[]
		}
	}

	componentDidMount(){

		let current_url = window.location.href;

		var access_token = current_url.match(/\#(?:access_token)\=([\S\s]*?)\&/);
		var refresh_token = current_url.match(/\&(?:refresh_token)\=([\S\s]*?)\&/);
		var username = current_url.match(/\&(?:account_username)\=([\S\s]*?)\&/);

		console.log(username);
		if(access_token !== null && refresh_token !== null ){
			this.setState({
				access_token:access_token[1],
				refresh_token:refresh_token[1],
				userAuthorized:true,
				username:username[1]
			},()=>{
				this.postedComments();
			})
		}
	}


	getImageID = (id) => {
		this.setState({imageID:id});
	}

	getComment = (text) => {
		this.setState({comment:text})
	}

	onClickLogout = () => {
		this.setState({
			userAuthorized:false
		})
	}

	getImage = () => {
		
		let { imageID } = this.state;
		let url = '3/gallery/'+ imageID;

	    if(imageID.length !==0){
	    	Api({
		    	url:url,
		    	method:'GET',
		    	headers: {
	                Authorization: 'Client-ID ' + Config.api_key,
	                Accept: 'application/json'
	            }
		    })
		    .then((response)=>{
		    	let { data, status } = response.data;
		    	if(status === 200){
		    		if(data.images_count > 0){
		    			this.setState({
			    			images:data.images,
			    			imageHeight:data.cover_height,
			    			imageWidth:data.imageWidth
			    		},()=>{
			    			this.postedComments();
			    		})
		    		}
		    	}else{
		    		alert('Image not found');
		    	}
		    }).catch((error)=>{
		    	alert('Somthing went wrong');
		    })
	    }else{
	    	alert('Please enter image id');
	    }
	}

	postComment = () => {
		let { comment, imageID, access_token } = this.state;
		
		let url = '3/comment?image_id='+imageID+'&comment='+comment;
	    
	    if(comment.length !== 0){
	    	Api({
		    	url:url,
		    	method:'POST',
		    	headers: {
	                Authorization: 'Bearer ' + access_token
	            }
		    })
		    .then((response)=>{
		    	let { data, status } = response.data;
		    	if(status == 200){
		    		this.postedComments();
		    	}
		    }).catch((error)=>{
		    	console.log(error)
		    })
	    }else{
	    	alert('Write somthing about post');
	    }
	}

	postedComments = () => {
		let { comment, imageID, access_token, username } = this.state;
		
		let url = '3/account/'+username+'/comments/';
	    Api({
	    	url:url,
	    	method:'GET',
	    	headers: {
                Authorization: 'Bearer ' + access_token,
            }
	    })
	    .then((response)=>{
	    	let { data, status } = response.data;
	    	if(status == 200){
	    		this.setState({
	    			userComments:data
	    		})
	    	}
	    }).catch((error)=>{
	    	console.log(error);
	    })
	}

	getImageVideo = () => {
		let { images, imageWidth, imageHeight } = this.state;

		if(images[0].type == 'image/jpeg' || images[0].type == 'image/gif'){
			return(
				<div align="center" style={{paddingTop:'5%'}}>
					<div>
						<img src={images[0].link} style={{width: imageWidth ,height:imageHeight}}/>
					</div>
      			</div>
			)
		}

		if(images[0].type == 'video/mp4'){
			return(
				<div align="center" style={{paddingTop:'5%'}}>
		  			<video width={imageWidth} height={imageHeight} controls>
						  <source src={images[0].link} type="video/mp4"/>
					</video>
	  			</div>
			)
		}
	}

	render(){

		let { images, imageID, comment, authorizeView, access_token, refresh_token, userAuthorized, userComments, imageWidth, imageHeight } = this.state;

		// console.log(userComments);
		return(
			<div className="col-md-12" style={{marginTop:'2%'}}>

			    { !userAuthorized &&
			    	<div align="center">
			    		<img src="https://miro.medium.com/proxy/1*6bqgBkbNo7kXLv2qXU6NHQ.jpeg" width="100" height="100"/>
			    		<div>
			    			<a href="https://api.imgur.com/oauth2/authorize?client_id=8a530ce346c4612&response_type=token">
						    	<button className="btn btn-success">Authenticate with Imgur</button>
						    </a>
			    		</div>
			    	</div>
				}

				{
					userAuthorized &&
					<div className="row" style={{paddingTop:'2%'}}>
						<div className="col-md-11">
						<FormControl 
							autoFocus
							placeholder="Please enter image id"
				            value={imageID}
				            onChange={e => this.getImageID(e.target.value)}
						/>
						</div>
						<div className="col-md-1">
							<Button onClick={this.getImage} primary>
								Get
							</Button>
						</div>
					</div>
				}

				{
		      		images.length != 0 && 
		      		this.getImageVideo()
		      	}

		      
		      	{
		      		images.length != 0 &&
		      		<div>
		      			<div className="row" style={{paddingTop:'5%'}}>
		      				Comment:
		      			</div>

		      			<div className="row" style={{paddingTop:'2%'}}>
			      			<div className="col-md-5">
			      				<FormControl 
									autoFocus
									placeholder="Post your comment"
						            value={comment}
						            onChange={e => this.getComment(e.target.value)}
								/>
			      			</div>
			      			<div className="col-md-1">
			      				<Button block  onClick={this.postComment}>
						          Post
						        </Button>
			      			</div>
			      		</div>

		      		</div>
		      	}


		      	{ userAuthorized &&
		      		<div style={{paddingTop:'2%'}}>
		      			<CommentBox
		      				logoutFunc={this.onClickLogout}
				      		comment={userComments}
				      	/>
		      		</div>
		      	}

		      	
			</div>
		)
	}
}

export default withRouter(Home);

